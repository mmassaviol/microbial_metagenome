tabquality = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	hidden(textInput("selectquality", label = "", value="trimmomatic")),box(title = "Trimmomatic", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("quality__trimmomatic_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		radioButtons("quality__trimmomatic_qc_score", label = "Quality score encoding", choices = list("phred33" = "-phred33", "phred64" = "-phred64"),  selected = "-phred64", width =  "auto"),

	box(title = "(optional) fastaWithAdapters file for ILLUMINACLIP parameter", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		selectInput("quality__trimmomatic_fastaWithAdapters_select",label = "Select where to find the file", selected = "server", choices = c("On server" = "server", "On your machine" = "local")),
		conditionalPanel(condition = "input.quality__trimmomatic_fastaWithAdapters_select == 'server'",
				tags$label("(optional) fastaWithAdapters file for ILLUMINACLIP parameter"),
				fluidRow(
					column(4,shinyFilesButton("shinyfiles_quality__trimmomatic_fastaWithAdapters",label="Please select a file", title="(optional) fastaWithAdapters file for ILLUMINACLIP parameter", multiple=FALSE)),
					column(8,textInput("quality__trimmomatic_fastaWithAdapters_server",label=NULL,value=""))
				)
			),
		conditionalPanel(condition = "input.quality__trimmomatic_fastaWithAdapters_select == 'local'",
			fileInput("quality__trimmomatic_fastaWithAdapters_local",label = "(optional) fastaWithAdapters file for ILLUMINACLIP parameter")
		)
	)
,

		textInput("quality__trimmomatic_illuminaclip", label = "(optional) See ILLUMINACLIP in documentation <seed mismatches>:<palindrome clip threshold>:<simple clip threshold>:<minAdapterLength>:<keepBothReads>", value = "", width = "auto"),

		textInput("quality__trimmomatic_otherparams", label = "(optional) See documentation for other trimmomatic parameters (LEADING, TRAILING, MINLEN, ...)", value = "", width = "auto"),

		p("Trimmomatic: A flexible read trimming tool for Illumina NGS data"),

		p("Website : ",a(href="http://www.usadellab.org/cms/?page=trimmomatic","http://www.usadellab.org/cms/?page=trimmomatic",target="_blank")),

		p("Documentation : ",a(href="http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf","http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.1093/bioinformatics/btu170","https://doi.org/10.1093/bioinformatics/btu170",target="_blank"))

	)))


