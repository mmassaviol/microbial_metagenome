tabassembly = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	hidden(textInput("selectassembly", label = "", value="megahit")),box(title = "MEGAHIT", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("assembly__megahit_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		numericInput("assembly__megahit_min_contig_len", label = "Minimum length of contigs to output", min = 1, max = NA, step = 1, width =  "auto", value = 200),

		p("MEGAHIT: An ultra-fast single-node solution for large and complex metagenomics assembly via succinct de Bruijn graph"),

		p("Website : ",a(href="https://github.com/voutcn/megahit","https://github.com/voutcn/megahit",target="_blank")),

		p("Documentation : ",a(href="https://github.com/voutcn/megahit/wiki","https://github.com/voutcn/megahit/wiki",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.1016/j.ymeth.2016.02.020","https://doi.org/10.1016/j.ymeth.2016.02.020",target="_blank"))

	)))


