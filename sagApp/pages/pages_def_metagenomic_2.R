tabmetagenomic_2 = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	hidden(textInput("selectmetagenomic_2", label = "", value="metabat")),box(title = "MetaBAT", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("metagenomic_2__metabat_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		numericInput("metagenomic_2__metabat_mincontig", label = "Minimum size of a contig for binning", min = 1500, max = NA, step = 1, width =  "auto", value = 2500),

		p("MetaBAT: Metagenome Binning based on Abundance and Tetranucleotide frequency"),

		p("Website : ",a(href="https://bitbucket.org/berkeleylab/metabat","https://bitbucket.org/berkeleylab/metabat",target="_blank")),

		p("Documentation : ",a(href="https://bitbucket.org/berkeleylab/metabat/wiki/Home","https://bitbucket.org/berkeleylab/metabat/wiki/Home",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.7717/peerj.1165","https://doi.org/10.7717/peerj.1165",target="_blank"))

	)))


