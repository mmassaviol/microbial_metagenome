FROM mbbteam/mbb_workflows_base:latest as alltools

RUN cd /opt/biotools \
 && wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.38.zip \
 && unzip Trimmomatic-0.38.zip \
 && echo -e '#!/bin/bash java -jar /opt/biotools/Trimmomatic-0.38/trimmomatic-0.38.jar' > bin/trimmomatic \
 && chmod 777 bin/trimmomatic \
 && rm Trimmomatic-0.38.zip

RUN apt -y update && apt install -y openjdk-8-jre

RUN cd /opt/biotools \
 && git clone https://github.com/voutcn/megahit.git \
 && cd megahit \
 && git submodule update --init \
 && mkdir build && cd build \
 && cmake .. -DCMAKE_BUILD_TYPE=Release \
 && make -j8 \
 && make simple_test \
 && make install \
 && cd /opt/biotools \
 && rm -rf megahit

RUN apt-get update \
 && cd /opt/biotools/bin \
 && wget https://raw.githubusercontent.com/tseemann/any2fasta/master/any2fasta \
 && chmod +x any2fasta

RUN apt-get install -y bioperl ncbi-blast+ libjson-perl libtext-csv-perl libpath-tiny-perl liblwp-protocol-https-perl libwww-perl \
 && cd /opt/biotools \
 && git clone --depth 1 --branch v1.0.1 https://github.com/tseemann/abricate.git \
 && ./abricate/bin/abricate --check \
 && ./abricate/bin/abricate --setupdb \
 && ./abricate/bin/abricate ./abricate/test/assembly.fa \
 && cd bin \
 && ln -s /opt/biotools/abricate/bin/abricate abricate

RUN cd /opt/biotools \
 && wget https://bitbucket.org/berkeleylab/metabat/downloads/metabat-static-binary-linux-x64_v2.12.1.tar.gz \
 && tar -xvzf metabat-static-binary-linux-x64_v2.12.1.tar.gz \
 && rm metabat-static-binary-linux-x64_v2.12.1.tar.gz \
 && cd bin \
 && ln -s /opt/biotools/metabat/metabat metabat

RUN apt-get update \
 && apt-get install -y zlib1g-dev pkg-config libfreetype6-dev libpng-dev python-matplotlib python-setuptools

RUN cd /opt/biotools \
 && wget https://github.com/ablab/quast/releases/download/quast_5.0.2/quast-5.0.2.tar.gz \
 && tar -zxvf quast-5.0.2.tar.gz \
 && rm quast-5.0.2.tar.gz \
 && cd quast-5.0.2/ \
 && python3 ./setup.py install_full

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY files /workflow
COPY sagApp /sagApp

